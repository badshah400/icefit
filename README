*INSTRUCTIONS FOR LINUX*


PRE-REQUISITES
You will need to have the following libraries (with respective header files)
installed:
1. plplot (C++ headers) >= 5.9.9
2. armadillo >= 3.2
3. gsl >= 1.14
4. python-numpy >= 1.6
5. python-scipy >= 0.11


USAGE
Run the provided script icefit.py from a bash terminal with the required
value of the spectral index α. Depending on your system, this would typically
involve the following steps from a bash terminal within the extracted directory.

Step 1. chmod +x icefit.py
Step 2. python test/self_test.py
If this reports "PASS", then the program is ready for use (as given in Step 3).
If it reports a status "FAIL", there was a problem with setting up the code on
your system.
Step 3. ./icefit.py <value of α>


OPTIONS
--trim
      icefit.py can take an integer value T for the optional argument --trim
      (-t) and
      trim the input data (IC data) by T (from the end). For example,
      > ./icefit.py --trim=1 2.0
      would fit the data assuming a spectral index of 2.0 and leaving out the
      data from the highes bin, i.e., the PeV events. The same run can also be
      given in any of the following ways:
      > ./icefit.py -t1 2.0
      > ./icefit.py --trim=1 2.0
      > ./icefit.py 2.0 -t1
      etc.


RESULTS

Results are stored in a new directory called results (directory is automatically
created if required). For a given α, e.g. 2.2,
the results consists of:
a) a data file named cascade_events_sig_back_2.2.dat with six cols
   i.   the bin minimum and
   ii.  maximum energies, 
   iii. normalised total events expected in corr. bin
   iv.  normalised events from GR and two
   v and vi. neutral current backgrounds and pure lepton signals from GR (last
        two columns irrelevant for present analysis)
b) a data file "Fit_Chisq_E_2.2.dat" which contains the value of least square
   χ² ovtained by fitting to the given α = 2.2.

IceCube data are stored in the directory IC_data. The code (in C++) to generate
un-normalised data for the given α is in ./src. The code to figure out the
normalisation from a least-square fit is in the directory ./lsq_fit (code written
in python).
