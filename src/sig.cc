//      sig.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include <cstdlib>
#include <cmath>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>

#include "misc.h"
#include "sig.h"
#include "monte.h"
#include "flux.h"
#include "cross_sec.h"

using namespace std;

extern const double NAV;
extern const double post_fac;
extern const size_t dims;

static id_params E_BAR_CC = {4,2};

static double (* flx) (const double, const unsigned int) = flux_power_gen;

double g(double *k, size_t dims, void *params)
{
	id_params tmp = *(id_params *)(params);
	double ret = dx_sec(k[0], k[1], tmp.x_sec_id);
	ret *= (10.0/18.0) * NAV * post_fac * 0.9
	       * flx(k[0],tmp.flux_id);

	return ret;
}

double sig_eve_E_had(double *x0, double *xf)
{
	double *xx0 = new double [dims];
	double *xxf = new double [dims];
	for(size_t i=0; i<dims; i++)
	{
		xx0[i] = x0[i];
		xxf[i] = xf[i];
	}
	xx0[0] = pow(10,x0[0]);
	xxf[0] = pow(10,xf[0]);
	gsl_monte_function G2 = { &g, dims, &E_BAR_CC };
	double res, err, res1;
	res = err = res1 = 0;
	aBc_vegas_int(xx0,xxf,G2,res1,err);
	delete [] xx0;
	delete [] xxf;
	res += res1;
	
	return res;
}


