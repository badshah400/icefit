#ifndef __MONTE_H_
#define __MONTE_H_

double aBc_vegas_int(double *xl, double *xu, gsl_monte_function G, double &res, double &err);

#endif
