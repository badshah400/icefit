#ifndef __INTEGRATION_H
#define __INTEGRATION_H

double f(double x, void *params);
void aBc_intg(const double x0, const double xf, gsl_function &F, double &res, double &err);


#endif
