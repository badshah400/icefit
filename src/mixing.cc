#include <cmath>
#include <armadillo>

using namespace std;
using namespace arma;

#include "mixing.h"

mixing_params::mixing_params(double s12, double s23, double s13, double dcp)
{
	sin_th12 = s12;
	sin_th23 = s23;
	sin_th13 = s13;
	del_cp   = dcp;
			
}

double mixing_params::cth12() const
{
	return sqrt(1 - sin_th12 * sin_th12);
}

double mixing_params::cth23() const
{
	return sqrt(1 - sin_th23 * sin_th23);
}

double mixing_params::cth13() const
{
	return sqrt(1 - sin_th13 * sin_th13);
}

vec prob(const mixing_params &p, mat & s, const vec &flux_ini)
{
	mat U(3,3);
	double s12  = p.sth12();
	double s23  = p.sth23();
	double s13  = p.sth13();
	double c12  = p.cth12();
	double c23  = p.cth23();
	double c13  = p.cth13();
	double cdcp = cos(p.delcp());
	U(0,0) = c12*c13; U(0,1) = s12*c13; U(0,2) = s13*cdcp;
	U(1,0) = -1.0*s12*c23 - c12*s23*s13*cdcp;
	U(1,1) = c12*c23 - s12*s23*s13*cdcp;
	U(1,2) = s23*c13;
	U(2,0) = s12*s23 - c12*c23*s13*cdcp;
	U(2,1) = -1.0*c12*s23 - s12*c23*s13*cdcp;
	U(2,2) = c23*c13;

	mat res(3,3);
	res = square(U) * s * square(trans(U));
	return (res * flux_ini);
}

double nu_e_prob_std(const mixing_params &p, const vec &v)
{
	mat idn = eye<mat>(3,3);
	vec tmp = prob(p,idn,v);
	return tmp(e);
}

double nu_mu_prob_std(const mixing_params &p, const vec &v)
{
	mat idn = eye<mat>(3,3);
	vec tmp = prob(p,idn,v);
	return tmp(mu);
}

double nu_tau_prob_std(const mixing_params &p, const vec &v)
{
	mat idn = eye<mat>(3,3);
	vec tmp = prob(p,idn,v);
	return tmp(tau);
}
