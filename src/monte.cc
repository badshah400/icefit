//      monte.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.


#include <cstdlib>
#include <iostream>
#include <gsl/gsl_math.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_vegas.h>
#include <gsl/gsl_const_num.h>

using namespace std;

#include "misc.h"
#include "monte.h"

extern const size_t dims;

double aBc_vegas_int(double *xl, double *xu, gsl_monte_function G, double &res,
		     double &err)
{
	const gsl_rng_type *T;
	gsl_rng *r;
	const size_t calls = 25000;

	gsl_rng_env_setup();

	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	{
		gsl_monte_vegas_state *s = gsl_monte_vegas_alloc(dims);
		gsl_monte_vegas_integrate(&G, xl, xu, dims, calls, r, s, &res,
					  &err);
		gsl_monte_vegas_free(s);
	}

	gsl_rng_free(r);

	return 0;
}
