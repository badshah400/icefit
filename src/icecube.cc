/*
 * icecube.cc
 * This file is part of uhecal
 *
 * Copyright (C) 2012 - Atri Bhattacharya
 *
 * uhecal is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * uhecal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uhecal. If not, see <http://www.gnu.org/licenses/>.
 */

#include <vector>
#include <cmath>
#include <iostream>

#include "icecube.h"
#include "interp1.h"
#include "stddat.h"

using namespace std;

vector<vector<double> > IC_in = stddatread("flux_data/ICLIMS.dat", 2);
interp_data IC_Lim1(IC_in);

double IC_Lims(double x)
{
//	for (unsigned int i = 0; i < IC_in.size(); i += 1)
//	{
//		cout << IC_in.at(i).at(0) << endl;
//	}
	double tmp  = -60.0;
	double absc = log10(x);
	bool chck = ((absc > IC_in.front().at(0)) 
	             && (absc < (IC_in.back().at(0) - 1e-1)));
	if (chck)
	{
		tmp = IC_Lim1.interp_linear(absc);
	}
	return (pow(10,tmp) / x / x);
}
