/*
 * interp1.cc
 * This file is part of atmosnu
 *
 * Copyright (C) 2012 - Atri Bhattacharya
 *
 * atmosnu is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * atmosnu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with atmosnu. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "interp1.h"

using namespace std;


interp_data::interp_data(const vector<vector<double> > & v, 
                         const unsigned int c2)
{
	dim = 2;
	dat = v.size() - 1;
	idat = new double * [dim];
	for (unsigned int i = 0; i < dim; i += 1)
	{
		idat[i] = new double [dat];
	}
	for (unsigned int i = 0; i < dat; i += 1)
	{
		idat[0][i] = v.at(i).at(0);
		idat[1][i] = v.at(i).at(c2);
	}
}

interp_data::interp_data(const interp_data & xi)
{
	dim = xi.dim;
	dat = xi.dat;
	idat = new double * [dim];
	for (unsigned int i = 0; i < dim; i += 1)
	{
		idat[i] = new double [dat];
	}
	for (unsigned int i = 0; i < dim; i += 1)
	{
		for (unsigned int j = 0; j < dat; j += 1)
		{
			idat[i][j] = xi.idat[i][j];
		}
	}
		
}


interp_data::~interp_data()
{
	for (unsigned int i = 0; i < dim; i += 1)
	{
		delete [] idat[i];
	}
	delete [] idat;
}

interp_data & interp_data::operator=(const interp_data & xi)
{
	for (unsigned int i = 0; i < dim; i += 1)
	{
		delete [] idat;
	}
	delete [] idat;
	dim = xi.dim;
	dat = xi.dat;
	idat = new double * [dim];
	for (unsigned int i = 0; i < dim; i += 1)
	{
		idat[i] = new double [dat];
	}
	for (unsigned int i = 0; i < dim; i += 1)
	{
		for (unsigned int j = 0; j < dat; j += 1)
		{
			idat[i][j] = xi.idat[i][j];
		}
	}
	
	return *this;

}


double interp_data::interp_akima(const double x0)
{
	double y0;
	{
		gsl_interp_accel *acc = gsl_interp_accel_alloc ();
		gsl_spline    *spline = gsl_spline_alloc (gsl_interp_akima, dat);
		gsl_spline_init (spline, idat[0], idat[1], dat);
	     
		y0 = gsl_spline_eval (spline, x0, acc);
		gsl_spline_free (spline);
		gsl_interp_accel_free (acc);
       }
       
       return y0;
 
}

double interp_data::interp_linear(const double x0)
{
	double y0;
	{
		gsl_interp_accel *acc = gsl_interp_accel_alloc ();
		gsl_spline    *spline = gsl_spline_alloc (gsl_interp_linear, dat);
		gsl_spline_init (spline, idat[0], idat[1], dat);
	     
		y0 = gsl_spline_eval (spline, x0, acc);
		gsl_spline_free (spline);
		gsl_interp_accel_free (acc);
       }
       
       return y0;
 
}
