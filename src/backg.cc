//      backg.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.


#include <cmath>
#include <gsl/gsl_integration.h>
#include <vector>
using namespace std;
#include "misc.h"
#include "cross_sec.h"
#include "integration.h"
#include "flux.h"
#include "backg.h"

static double (* flx) (const double, const unsigned int) = flux_power_gen;

static double func1(double x, void *pt);
static double func0(double x, void *pt);

static id_params E_CC       = {1,1};
static id_params E_NC       = {1,3};
static id_params E_BAR_CC   = {4,2};
static id_params E_BAR_NC   = {4,4};
static id_params NU_NC      = {2,3};
static id_params NU_BAR_NC  = {5,4};
static id_params TAU_CC     = {3,1};
static id_params TAU_BAR_CC = {6,2};
static id_params TAU_NC     = NU_NC;
static id_params TAU_BAR_NC = NU_BAR_NC;
static id_params MU_CC      = {2,1};
static id_params MU_BAR_CC  = {5,2};

static const double thres = 1e-3;

double backg_eve_NC(double EN_o, double EN_f)
{
	double res1(0), err1(0);
	gsl_function tmp;
	tmp.function = &func0;

	double res = 0.0;
	vector<gsl_function> F;

	tmp.params   = &E_NC;
	F.push_back(tmp);

	tmp.params   = &E_BAR_NC;
	F.push_back(tmp);

	tmp.params   = &NU_NC;
	F.push_back(tmp);

	tmp.params   = &NU_BAR_NC;
	F.push_back(tmp);

	tmp.params   = &TAU_NC;
	F.push_back(tmp);

	tmp.params   = &TAU_BAR_NC;
	F.push_back(tmp);
	
	double x0 = pow(10.0,EN_o)/0.26;
	double xf = pow(10.0,EN_f)/0.26;
	for(unsigned int i=0; i<F.size(); i++)
	{
		aBc_intg(x0,xf,F.at(i),res1,err1);
		res += res1;
	}
	
	return res;     
}

double backg_eve_E_CC(double EN_o, double EN_f)
{
	double res1, err1;
	res1 = err1 = 0.0;
	gsl_function tmp;
	tmp.function = &func1;

	double res = 0.0;
	vector<gsl_function> F;

	tmp.params   = &E_CC;
	F.push_back(tmp);

	tmp.params   = &E_BAR_CC;
	F.push_back(tmp);

	double x0 = pow(10.0,EN_o);
	double xf = pow(10.0,EN_f);
	for(unsigned int i=0; i<F.size(); i++)
	{
		aBc_intg(x0,xf,F.at(i),res1,err1);
		res += res1;
	}
	
	return res;     
}

double backg_eve_MU_CC(double EN_o, double EN_f)
{
	double res1, err1;
	res1 = err1 = 0.0;
	gsl_function tmp;
	tmp.function = &func1;

	double res = 0.0;
	vector<gsl_function> F;

	tmp.params   = &MU_CC;
	F.push_back(tmp);

	tmp.params   = &MU_BAR_CC;
	F.push_back(tmp);

	double x0 = pow(10.0,EN_o);
	double xf = pow(10.0,EN_f);
	for(unsigned int i=0; i<F.size(); i++)
	{
		aBc_intg(x0,xf,F.at(i),res1,err1);
		res += res1;
	}
	
	return res;     
}

double backg_eve_TAU_CC(double EN_o, double EN_f)
{
	double res1, err1;
	res1 = err1 = 0.0;
	gsl_function tmp;
	tmp.function = &func1;

	vector<gsl_function> F;

	tmp.params   = &TAU_CC;
	F.push_back(tmp);

	tmp.params   = &TAU_BAR_CC;
	F.push_back(tmp);
	
	double res = 0.0;
	if(EN_o < 6.3)
	{
		res = 0.0;
		double x0 = pow(10.0,EN_o);
		double xf = pow(10.0,EN_f);
		for(unsigned int i=0; i<F.size(); i++)
		{
			aBc_intg(x0,xf,F.at(i),res1,err1);
			res += res1;
		}
	}
	else if(EN_o < 6.3+thres && EN_o > 6.3-thres)
	{
		res = 0.0;
		double x0 = pow(10.0,EN_o);
		double xf = pow(10.0,6.45);
		for(unsigned int i=0; i<F.size(); i++)
		{
			aBc_intg(x0,xf,F.at(i),res1,err1);
			res += res1;
		}
	}
	else
	{
		res = 0.0;
	}
	
	return res;     
}

static double func0(double x, void *pt)
{
	id_params chk = *(id_params *)(pt);
	
	double fc1;
	fc1 = 0.9 * flx(x,chk.flux_id)*x_sec(x,chk.x_sec_id)*NAV*post_fac;
	
	return fc1;
}

static double func1(double x, void *pt)
{
	id_params chk = *(id_params *)(pt);
	
	double fc1;
	fc1 = 0.9 * flx(x,chk.flux_id)*x_sec(x,chk.x_sec_id)*NAV*post_fac;
	
	return fc1;
}
