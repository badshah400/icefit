//      flux.cc
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include <iostream>
#include <cmath>
#include <armadillo>
using namespace std;
using namespace arma;
#include "misc.h"
#include "stddat.h"
#include "interp1.h"
#include "mixing.h"
#include "flux.h"
#include "stddat.h"
#include "interp1.h"
#include "backg.h"


static const double interp_lim_thres = 0.12;
static const mixing_params mp = bfit_params;
static const long double fixed_nue_flux = 3.86365e-09;

enum {E_CC = 0, ALL_CC};
const unsigned int SCALE_EVENTS = E_CC;
const double yr2expos = 662.0/365.0;
const double dratio_3max = 20.956620084556224;
const double dratio_2max = 8.224688799689218;
const double dratio_3min = 2.8805403547159756;
const double dratio_2min = 3.215709148238065;
const double dratio_bfit = 1.1333617 / 2.1285923e-01;

double fx_nue(double);
double fx_anue(double);
double fx_numu(double);

vector<vector<double> > nue_in   = stddatread("flux_data/fnue.dat",  2);
vector<vector<double> > numu_in  = stddatread("flux_data/fnumu.dat", 2);
vector<vector<double> > anue_in  = stddatread("flux_data/fanue.dat", 2);

interp_data  nue_intrp(nue_in);
interp_data anue_intrp(anue_in);
interp_data numu_intrp(numu_in);

static const double alpha = 2.3;

double flux_power_gen(const double x, const unsigned int chk)
{
	double res(0), z(0);
	if (x > 1e4 && x < 1e7)
	{
		z = 1.0e-2 / pow(x, alpha);
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}

	switch(SCALE_EVENTS)
	{
		case E_CC:
			if (alpha == 2.3)
				res /= (3.6566093e+05 / 6.1208);
			else if (alpha == 2.0)
				res /= (8153830.11255228 / 6.1208);
			else if (alpha == 2.2)
				res /= (1.0290346e+06 / 8);
			//	res /= (1.0290346e+06 / 6.1208);
			else if (alpha == 2.4)
				res /= (1.2995807e+05 / 6.1208);
			else if (alpha == 3.0)
				res /= (2.6286931e+02 / 6.1208);
			
//			res /= (29834.6548112297 * 22.29888579142447 * 1.538427282);
			break;
		case ALL_CC:
			res /= 1;
			break;
		default:
			break;
	}


	return res;

}

double flux_E2(const double x, const unsigned int chk)
{
	double res(0), z(0);
	if (x > 1e4 && x < 1e7)
	{
		z = 1.0e-8 / x / x;
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}
	
	switch(SCALE_EVENTS)
	{
		case E_CC:
			res /= 0.1669861;
			break;
		case ALL_CC:
			res /= 0.3151144;
			break;
		default:
			break;
	}

	return res;

}

double flux_E2_5(const double x, const unsigned int chk)
{
	double res(0), z(0);
	if (x > 1e4 && x < 1e7)
	{
		z = 1.0e-4 / pow(x, 2.5);
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}
	
	switch(SCALE_EVENTS)
	{
		case E_CC:
			res /= 1.5794028;
			break;
		case ALL_CC:
			res /= 2.9804046;
			break;
		default:
			break;
	}

	return res;

}

double flux_E3(const double x, const unsigned int chk)
{
	double res(0), z(0);
	if (x > 1e4 && x < 1e7)
	{
		z = 1.0e-2 / pow(x, 3);
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}
	
	switch(SCALE_EVENTS)
	{
		case E_CC:
			res /= 0.1495490;
			break;
		case ALL_CC:
			res /= 0.2822022;
			break;
		default:
			break;
	}

	return res;

}

double flux_E3_5(const double x, const unsigned int chk)
{
	double res(0), z(0);
	if (x > 1e4 && x < 1e7)
	{
		z = 1.0e2 / pow(x, 3.5);
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}
	
	switch(SCALE_EVENTS)
	{
		case E_CC:
			res /= 1.4175903 ;
			break;
		case ALL_CC:
			res /= 2.6749892;
			break;
		default:
			break;
	}

	return res;

}

double flux_E4(const double x, const unsigned int chk)
{
	double res(0), z(0);
	if (x > 1e4 && x < 1e7)
	{
		z = 1.0e5 / pow(x, 4);
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}
	
	switch(SCALE_EVENTS)
	{
		case E_CC:
			res /= 1.3452159 ;
			break;
		case ALL_CC:
			res /= 2.5383868;
			break;
		default:
			break;
	}

	return res;

}

double flux_razzaque_grb(const double x, const unsigned int chk)
{
	double res(0), z(0);
	if (x > 1e4 && x < 1e7)
	{
		z = 1.0e-8 / x / x;
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}
	
	switch(SCALE_EVENTS)
	{
		case E_CC:
			res /= 0.1669861;
			break;
		case ALL_CC:
			res /= 0.3151144;
			break;
		default:
			break;
	}

	return res;
}

vector<vector<double> > stkdata = stddatread("stk-001.dat", 2);

double flux_stecker_agn(const double x, const unsigned int chk)
{
	double res(0);
	interp_data id1(stkdata);
	
	double z = 0;
	if (x > stkdata.front().at(0)
	     && x < stkdata.at(stkdata.size() - 2).at(0))
	{
		z = 0.5 * id1.interp_akima(x);
	}
	
	vec pg_nu(3), pg_anu(3);
	pg_nu(0)  = 1.0; pg_nu(1)  = 1.0; pg_nu(2)  = 0.0; 
	pg_anu(0) = 0.0; pg_anu(1) = 1.0; pg_anu(2) = 0.0; 
	
	vec fnu  = pg_nu;
	vec fanu = pg_anu;
	
	switch(chk)
	{
		case 1:
			res = z * nu_e_prob_std(mp, fnu);
			break;
		case 2:
			res = z * nu_mu_prob_std(mp, fnu);
			break;
		case 3:
			res = z * nu_tau_prob_std(mp, fnu);
			break;
		case 4:
			res = z * nu_e_prob_std(mp, fanu);
			break;
		case 5:
			res = z * nu_mu_prob_std(mp, fanu);
			break;
		case 6:
			res = z * nu_tau_prob_std(mp, fanu);
			break;
		default:
			res = 0.0;
	}
	
	switch(SCALE_EVENTS)
	{
		case E_CC:
			res /= 10.1974555;
			break;
		case ALL_CC:
			res /= 19.2435419;
			break;
		default:
			break;
	}
	
	return res;
}

double flux_val(const double x, const unsigned int chk)
{
	flux_params fp1;
	fp1.param_x = 0.0;
	
	vec pg_nu(3), pg_antinu(3);
	pg_nu(0) = 0.0; pg_nu(1) = 0.0; pg_nu(2) = 0.0; 
	pg_antinu(0) = 1.0; pg_antinu(1) = 0.0; pg_antinu(2) = 0.0; 

	
	vec fnu = pg_nu;
	vec fanu = pg_antinu;
	
	double res;

	switch(chk)
	{
		case 1:
//			fp1.osc_r = nu_e_prob_std(mp, fnu);
//			res = f3(x,fp1);
			res = 3.9e-9 / x / x;
			break;
		case 2:
//			fp1.osc_r = nu_mu_prob_std(mp, fnu);
//			res = f3(x,fp1);
			res = 3.9e-9 / x / x;
			break;
		case 3:
//			fp1.osc_r = nu_tau_prob_std(mp, fnu);
//			res = f3(x,fp1);
			res = 3.9e-9 / x / x;
			break;
		case 4:
//			fp1.osc_r = nu_e_prob_std(mp, fanu);
//			res = f3(x,fp1);
			res = 3.9e-10 / x / x;
			break;
		case 5:
//			fp1.osc_r = nu_mu_prob_std(mp, fanu);
//			res = f3(x,fp1);
			res = 3.9e-9 / x / x;
			break;
		case 6:
//			fp1.osc_r = nu_tau_prob_std(mp, fanu);
//			res = f3(x,fp1);
			res = 3.9e-9 / x / x;
			break;
		default:
			res = 0.0;
			
	}
	return (res * 40.521);// * 70.32471648;
}

double f3(double x, flux_params &p)
{
	long double tmp = (1-p.param_x)*p.osc_r*0.25/3.0;
	tmp += (p.param_x*0.6/6);
	tmp *= 6.0e-8;
	long double sfact = 0.701961068;
	tmp /= (x * x);
//	cout << sfact << endl;
	if (sfact)
	{
		tmp /= sfact;
	}
	return (tmp * 1.083655913);
}

//double csmg_flux(double x, int chk)
//{
//	switch(chk)
//	{
//		case 1: 
//	}
//}

double fx_nue(const double x)
{
	double tmp  = -60;
	double absc = log10(x) + 9; 
	bool chck = ((absc > nue_in.front().at(0)) 
	             && (absc < nue_in.back().at(0) - interp_lim_thres));
	if (chck)
	{
//		cout << absc << '\t' << nue_in.back().at(0) << endl;
		tmp = nue_intrp.interp_linear(absc);
	}
	
	return (pow(10,tmp) / x);
}

double fx_numu(const double x)
{
	double tmp  = -60;
	double absc = log10(x) + 9; 
	bool chck = ((absc > numu_in.front().at(0)) 
	             && (absc < numu_in.back().at(0) - interp_lim_thres));
	             
	if (chck)
	{
//		cout << absc << '\t' << numu_in.back().at(0) << endl;
		tmp = numu_intrp.interp_linear(absc);
	}
	return (pow(10,tmp) / x);
}

double fx_anue(const double x)
{
	double tmp  = -60.0;
	double absc = log10(x) + 9;
	bool chck = ((absc > (anue_in.front().at(0))) 
	             && (absc < anue_in.back().at(0) - interp_lim_thres));
	if (chck)
	{
//		cout << absc << '\t' << anue_in.back().at(0) << endl;
		tmp = anue_intrp.interp_linear(absc);
	}
	return (pow(10,tmp) / x);
}
