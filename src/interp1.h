#ifndef __INTERP1_H__
#define __INTERP1_H__

#include <vector>

class interp_data
{
	private:
	double ** idat;
	unsigned int dim;
	unsigned int dat;
	unsigned int no_data_flag;
	
	public:
	interp_data(const interp_data &);
	interp_data(const std::vector<std::vector<double> > &, const unsigned int = 1);
	~interp_data();
	
	interp_data & operator=(const interp_data &);
	
	double interp_akima(const double);
	double interp_cspline(const double);
	double interp_linear(const double);
};



#endif /* __INTERP1_H__ */

