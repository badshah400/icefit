#ifndef __CROSS_SEC_H_
#define __CROSS_SEC_H_

struct x_sec_params
{
	double pre_fac;
	double index;
};


double x_sec(double x, int chk);
double f2(double x, x_sec_params &p);
double dx_sec(double E_Nu, double y, int chk);

#endif
