//      integration.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.


#include <cmath>
#include <gsl/gsl_integration.h>

using namespace std;

#include "integration.h"

void aBc_intg(const double x0, const double xf, gsl_function &F, double &res, double &err)
{
	const int divs = 1E3;
	const double relerr = 1.0e-5;

	gsl_integration_workspace *w 
		= gsl_integration_workspace_alloc (divs);
	gsl_integration_qag
		(&F, x0, xf, relerr, relerr, divs, 1, w, &res, &err);
		
	return;	

}

