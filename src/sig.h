#ifndef __SIG_H_
#define __SIG_H_

double g(double  *kern, size_t dims, void *params);
double sig_eve_E_had(double *x0, double *xf);

#endif
