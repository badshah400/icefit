#ifndef __MIXING_H_
#define __MIXING_H_

#include <armadillo>

class mixing_params
{
	private:
	double sin_th12;
	double sin_th23;
	double sin_th13;
	double del_cp;
	
	public:
	mixing_params(double, double, double, double);
	inline double sth12() const {return sin_th12;};
	inline double sth23() const {return sin_th23;};
	inline double sth13() const {return sin_th13;};
	inline double delcp() const {return del_cp;};
	double cth12() const;
	double cth23() const;
	double cth13() const;

};

enum flavs{e=0,mu,tau};

arma::vec prob(const mixing_params &, arma::mat &, const arma::vec &);
double nu_e_prob_std(const mixing_params &, const arma::vec &) ;
double nu_mu_prob_std(const mixing_params &, const arma::vec &);
double nu_tau_prob_std(const mixing_params &, const arma::vec &);

const mixing_params tri_bi_params(sin(34.0*arma::math::pi()/180.0), sin(45.0*arma::math::pi()/180.0), 0.0, 0.0);
const mixing_params bfit_params(sqrt(0.307), sqrt(0.386), sqrt(0.0241), 1.08 * arma::math::pi());
const mixing_params min_params(sqrt(0.259), sqrt(0.331), sqrt(1.69e-2), 0.77 * arma::math::pi());
const mixing_params max_params(sqrt(0.359), sqrt(0.637), sqrt(3.13e-2), 1.36 * arma::math::pi());

#endif
