#ifndef __MISC_H_
#define __MISC_H_

#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_const_num.h>

struct id_params
{
	int flux_id;
	int x_sec_id;
};

enum {EL = 0, MU, TAU};

const double Fermi       = 1.16632e-5;
const double E_mass      = 0.511*1.0e-3;
const double Mu_mass     = 105.658389*1.0e-3;
const double W_mass      = 80.22;
const double W_rate      = 2.08;

const double NAV         = GSL_CONST_NUM_AVOGADRO * 1.0e15;
const double post_fac    = 2*M_PI*3.2e7;
const double CGS_2_NAT_L = 1.97327e-5;

const size_t dims = 2;


#endif
