//      flux.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include <iostream>
#include <cmath>
#include <armadillo>
using namespace std;
using namespace arma;
#include "mixing.h"
#include "flux.h"
#include "stddat.h"
#include "interp1.h"

static const mixing_params mp = bfit_params;

double f3(double x, flux_params &p);
double fx_nue(double);
double fx_anue(double);
double fx_numu(double);

vector<vector<double> > nue_in   = stddatread("flux_data/fnue.dat" , 2);
vector<vector<double> > numu_in  = stddatread("flux_data/fnumu.dat", 2);
vector<vector<double> > anue_in  = stddatread("flux_data/fanue.dat", 2);

interp_data  nue_intrp(nue_in);
interp_data anue_intrp(anue_in);
interp_data numu_intrp(numu_in);

double flux_val(double x, int chk)
{
	flux_params fp1;
	fp1.param_x = 0.0;
	
	vec pg_nu(3), pg_antinu(3);
	pg_nu(0) = 0.0; pg_nu(1) = 0.0; pg_nu(2) = 0.0; 
	pg_antinu(0) = 1.0; pg_antinu(1) = 0.0; pg_antinu(2) = 0.0; 

	
	vec fnu = pg_nu;
	vec fanu = pg_antinu;
	
	double res;

	switch(chk)
	{
		case 1:
			res = fx_nue(x);
			break;
		case 2:
			res = fx_numu(x);
			break;
		case 3:
			res = fx_numu(x);
			break;
		case 4:
			res = fx_anue(x);
			break;
		case 5:
			res = fx_numu(x);
			break;
		case 6:
			res = fx_numu(x);
			break;
		default:
			res = 0.0;
			
	}
	return (res * 40.520334448);// * 70.32471648;
}

double f3(double x, flux_params &p)
{
	double tmp = (1-p.param_x)*p.osc_r*0.25/3.0;
	tmp += (p.param_x*0.6/6);
	tmp *= (6.0e-8/(x*x)) / 1.40392;
	return tmp;
}

double fx_nue(double x)
{
	double tmp  = -60;
	double absc = log10(x) + 9; 
	bool chck = ((absc > nue_in.front().at(0)) 
	             && (absc < (nue_in.back().at(0) - 1e-1)));
	if (chck)
	{
		tmp = nue_intrp.interp_linear(absc);
	}
	return (pow(10,tmp) / x);
}

double fx_numu(double x)
{
	double tmp  = -60;
	double absc = log10(x) + 9; 
	bool chck = ((absc > numu_in.front().at(0)) 
	             && (absc < (numu_in.back().at(0) - 1e-1)));
	if (chck)
	{
		tmp = numu_intrp.interp_linear(absc);
	}
	return (pow(10,tmp) / x);
}

double fx_anue(double x)
{
	double tmp  = -60.0;
	double absc = log10(x) + 9;
	bool chck = ((absc > anue_in.front().at(0)) 
	             && (absc < (anue_in.back().at(0) - 1e-1)));
	if (chck)
	{
		tmp = anue_intrp.interp_linear(absc);
	}
	return (pow(10,tmp) / x);
}
