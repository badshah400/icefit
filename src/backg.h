#ifndef __BACKG_H_
#define __BACKG_H_

double backg_eve_NC(double, double);
double backg_eve_TAU_CC(double, double);
double backg_eve_E_CC(double, double);
double backg_eve_MU_CC(double, double);

#endif
