#ifndef __FLUX_H_
#define __FLUX_H_

struct flux_params
{
	double param_x;
	double osc_r;
};

double flux_val(const double, const unsigned int);
double f3(double, flux_params &);

double flux_power_gen(const double, const unsigned int);
double flux_E2(const double, const unsigned int);
double flux_E2_5(const double, const unsigned int);
double flux_E3(const double, const unsigned int);
double flux_E3_5(const double, const unsigned int);
double flux_E4(const double, const unsigned int);

double flux_stecker_agn(const double, const unsigned int);

double flux_razzaque_grb(const double, const unsigned int);

#endif
