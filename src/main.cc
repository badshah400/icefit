//      main.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 3 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.


#include <iostream>
#include <fstream>
#include <cstdlib>
#include <gsl/gsl_integration.h>
#include <vector>
#include <algorithm>
#include <cmath>

#include "stddat.h"
#include "flux.h"
#include "cross_sec.h"
#include "backg.h"
#include "sig.h"
#include "integration.h"
#include "plot.h"
#include "icecube.h"

using namespace std;

double func_back1(double db);
double func_back2(double db);
double func_back3(double db);
double func_back4(double db);
double func_sig_shower(double db);
double func_sig_mutrack(double db);

const double incr = 0.2;

static const double thres = 1.0e-3;

//~ Strings for plot legends
const char *leg_txt[] = { "Total shower events from all flavours",
//			  "#fs#gn#fn#de#u CC events",
//                          "#gn#d#gt#u CC events",
                          "Cascade events due to GR",
                          "CC events from #gn#de#uN interactions",
                          "CC events from #gn#d#gm#uN interactions"
			};


//~ Plot Label
const char *plot_title = "#<bold/>Events at IceCube";


int main(int argv, const char **argc)
{
//	for (double ENN = 4.0; ENN < 7.0; ENN += 0.2)
//	{
//		double x = pow(10, ENN);
//		double res = flux_E2(x, 1) + flux_E2(x, 4);
//		res /= (flux_E2(x, 2) + flux_E2(x, 5));
//		cout << ENN << '\t' << res << endl;
//	}
	
	vector <vector<double> > IC_DAT
	 = stddatread("flux_data/IC_Data_20131108.dat", 2);
	
	vector<double> IC_EVE;
	for (unsigned int i = 0; i < IC_DAT.size() - 1; i++)
	{
		IC_EVE.push_back(IC_DAT[i][1]);
	}
	
	vector <double> EN_list, res_back1, res_back2, res_back3, res_back4,
	                res_sig_sh, res_sig_mt, res;
	for(double tmp_EN = 4.0; tmp_EN <= 7 + thres; tmp_EN += incr)
	{
		EN_list.push_back(tmp_EN);
	}
	res_back1.resize(EN_list.size());
	res_back2.resize(EN_list.size());
	res_back3.resize(EN_list.size());
	res_back4.resize(EN_list.size());
	res.resize(EN_list.size());
	res_sig_sh.resize(EN_list.size());
	res_sig_mt.resize(EN_list.size());
	
	ofstream fout("../cascade_events_sig_back.dat");
	
	transform(EN_list.begin(), EN_list.end(), res_back1.begin(), func_back1);
	transform(EN_list.begin(), EN_list.end(), res_back2.begin(), func_back2);
	transform(EN_list.begin(), EN_list.end(), res_back3.begin(), func_back3);
	transform(EN_list.begin(), EN_list.end(), res_back4.begin(), func_back4);
	transform(EN_list.begin(), EN_list.end(), res_sig_sh.begin(), func_sig_shower);
	transform(EN_list.begin(), EN_list.end(), res_sig_mt.begin(), func_sig_mutrack);
	
	for(unsigned int i=0; i<res_back1.size()-1; i++)
	{
		fout.setf(ios::scientific, ios::floatfield);		
		fout.precision(5);
		fout << pow(10,EN_list.at(i));
		fout.width(15);
		fout << pow(10,EN_list.at(i+1));
		fout.precision(7);
		fout.width(15);
		res.at(i) = res_sig_sh.at(i) +
		            res_back1.at(i) + res_back3.at(i) +
			    res_back4.at(i);
		fout << res.at(i);
		fout.width(15);
		fout << res_sig_sh.at(i);
		fout.width(15);
		fout << res_back1.at(i);
		fout.width(15);
		fout << res_back4.at(i) << endl;
		fout.unsetf(ios::floatfield);	
	}
	fout.close();

//	vector <vector <double> >  res_all;
//	res_all.push_back(res);
//	res_all.push_back(res_sig_sh);
//	res_all.push_back(res_back1);
//	res_all.push_back(res_back4);
//	res_all.push_back(IC_EVE);
//
//	PLOT1 x( argv, argc, EN_list, res_all );
//	
	return 0;
}

double func_sig_shower(double db)
{
	double x0[] = { db, 0.0 };
	double xf[] = { db+incr, 1.0 };
	double res = 0;
	res += (sig_eve_E_had(x0,xf));
	return res;
}

double func_sig_mutrack(double db)
{
	double x0[] = { db, 0.0 };
	double xf[] = { db+incr, 1.0 };
	double res = 0;
	res += (0.1578 * sig_eve_E_had(x0,xf));
	return res;
}

double func_back1(double db)
{
	double res = 0;
	res += backg_eve_E_CC(db, db+incr);
	
	return res;
}

double func_back2(double db)
{
	double res = 0;
	res += backg_eve_NC(db, db+incr);
	
	return res;
}

double func_back3(double db)
{
	double res = 0;
	res += backg_eve_TAU_CC(db, db+incr);
	
	return res;
}

double func_back4(double db)
{
	double res = 0;
	res += backg_eve_MU_CC(db, db+incr);
	
	return res;
}
