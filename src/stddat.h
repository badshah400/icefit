#ifndef __STDDAT_H__
#define __STDDAT_H__

#include <vector>
#include <string>

void stddatdisp(const std::vector <std::vector <double> > &);
std::vector <std::vector <double> > stddatread(const std::string &, const unsigned int = 1);

#endif /* __STDDAT_H__ */
