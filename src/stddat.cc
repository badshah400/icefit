/*
 * stddat.cc
 * This file is part of atmosnu
 *
 * Copyright (C) 2012 - Atri Bhattacharya
 *
 * atmosnu is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * atmosnu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with atmosnu. If not, see <http://www.gnu.org/licenses/>.
 */

 

#include <fstream>
#include <iostream>
#include <climits>

#include "stddat.h"

using namespace std;

vector <vector <double> > stddatread(const string & fname, const unsigned int ncols)
{
	vector <vector<double> > dat;
	ifstream fin(fname.c_str());
	
	double x;
	
	while(fin.good())
	{
		vector <double> vv;
		if(fin.peek() == '#' || fin.peek() == '\n')
		{
			fin.ignore(INT_MAX, '\n');			
		}
		else
		{
			for(unsigned int ii = 0; ii < ncols; ii++)
			{
				fin >> x;
				if (!fin.fail())
				{
					vv.push_back(x);
				}
			}
			dat.push_back(vv);
			fin.ignore(INT_MAX, '\n');
		}
	}
	
	fin.close();
	
	return dat;
}

void stddatdisp(const vector <vector <double> > & v)
{
	for (unsigned int i = 0; i < v.size(); i += 1)
	{
		for (unsigned int j = 0; j < v.at(i).size(); j += 1)
		{
			cout.setf(ios_base::fixed, ios_base::floatfield);
			if (j)
			{
				cout.precision(7);
				cout.width(20);
			}
			else
			{
				cout.precision(3);
				cout.width(6);
			}
			cout << v.at(i).at(j);
		}
		cout << endl;
	}
	
	return;

}
