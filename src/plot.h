#ifndef __PLOT_H_
#define __PLOT_H_

#include <plplot/plstream.h>
#include <vector>

using std::vector;

class PLOT1
{
	private:
	plstream *pls;
	
	public:
	PLOT1(int, const char**, const vector <double>,
	                         const vector <vector<double> >);
	void do_plot(const vector <double>, 
	             const vector <vector<double> >);
		
};


#endif
