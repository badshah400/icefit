//      cross_sec.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include <cmath>
#include <cstdlib>
#include <gsl/gsl_const_num.h>
#include "misc.h"
#include "cross_sec.h"
using namespace std;

extern const double Fermi;
extern const double E_mass;
extern const double W_mass;
extern const double Mu_mass;
extern const double W_rate;


double x_sec(double x, int chk)
{
	x_sec_params pt;

	switch(chk)
	{
		case 1: 
			pt.pre_fac = 2.69e-36;
			pt.index   = 0.402; 
			return f2(x,pt);
			break;
		
		case 2: 
			pt.pre_fac = 2.53e-36;
			pt.index   = 0.404;
			return f2(x,pt);
			break;

		case 3: 
			pt.pre_fac = 1.06e-36;
			pt.index   = 0.408;
			return f2(x,pt);
			break;
		
		case 4: 
			pt.pre_fac = 0.98e-36;
			pt.index   = 0.410;
			return f2(x,pt);
			break;
		
		default: return -1.0;	
	}
}

double dx_sec(double E_Nu, double y, int chk)
{
	double
	  GeV_inv_2_CGS = CGS_2_NAT_L*1e-9;
	double pre      = (Fermi*Fermi*E_mass)/(2*M_PI);
	pre            *= E_Nu;
	double val1     = Mu_mass*Mu_mass - E_mass*E_mass;
	val1            = 1 - val1/(2*E_mass*E_Nu);
	val1           *= val1;
	val1            = 4*(1-y)*(1-y)*val1;
	double den      = 1 - 2*E_mass*E_Nu/(W_mass*W_mass);
	den            *= den;
	den            += W_rate*W_rate/(W_mass*W_mass);
	
	double g        = pre*val1/den;
	double W_br     = 6.0;
	g              *= W_br;
	g              *= (GeV_inv_2_CGS*GeV_inv_2_CGS);
	
	return g;

}

double f2(double x, x_sec_params &p)
{
	double tmpf = p.pre_fac*(pow(x,p.index));
	return tmpf;
}

