//      plot.cxx
//      
//      Copyright 2011 Atri <badshah400@aim.com>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include <iostream>
#include <cmath>
#include <string>
#include <sstream>
#include <vector>
//#include <boost/lexical_cast.hpp>
#include <plplot/plstream.h>
#include "plot.h"

const unsigned int nl = 3;

extern const char *leg_txt[];

using namespace std;

//Plot configuration:
const string x_label  = "Log#d10#u(E / GeV)";
const string y_label  = "Events per 662 days";
const string pl_title = "Spectral index \u03b1 = 2.3";

PLOT1::PLOT1(int argv, const char** argc,
             const vector <double> x,
	     const vector <vector<double> > y)
{
	pls = new plstream();

	// Parse and process command line arguments.

	pls->parseopts( &argv, argc, PL_PARSE_FULL );
	//~ For printer-friendly plots, make background white
	//~ by uncommenting next three lines
	pls->scolbg(255,255,255);
	pls->scol0(1,0,0,0);
	pls->scol0(15,255,0,0);
	
	// Initialize plplot.
	pls->init();
	do_plot(x,y);
	delete pls;

}

static double smartlog10(const double x)
{
	double res;
	if (x)
	{
		res = log10(x);
	}
	else
	{
		res = 0;
	}
	return res;
}

void PLOT1::do_plot(const vector <double> x, 
                    const vector <vector<double> > y
                   )
{
	double a[x.size()], b[y.size()][x.size()];
	for (unsigned int i=0; i<x.size(); i++)
	{
		a[i]    = x.at(i);
		for(unsigned int j=0; j < y.size(); j++)
		{
			b[j][i] = y.at(j).at(i);
		}
	}
//	Set up viewport (or canvas)
	pls->schr( 0, 0.95 );
//	pls->env( 5.9, 7.3, 0, 1.0, 0, 00 );
//	To use ticks different from the default use following
//	instead of env()
	pls->adv( 0 );
	pls->vsta();
	pls->wind( 4.4, 7.0, 0, 12 );
//	pls->box( "bnit", 0.1, 0, "bnstv", 0.5, 0);
//	pls->box( "ct", 0.1, 0, "cst", 0.5, 0);

	pls->col0( 1 );			// Make 1 for plots on light background
	pls->lab(x_label.c_str(), 
	         y_label.c_str(),
	         pl_title.c_str()
	         );

//	Legend:
	pls->scol0a( 13, 255,   0, 250, 0.65 );
	pls->scol0a(  3,  70, 150, 255, 0.75 );
	pls->scol0a( 14, 190, 170,  40, 0.75 );
	pls->scol0a(  6,  75,  75,  75, 0.90 );
	pls->scol0a( 12, 200,   0,   0, 0.75 );

//	int textcol[nl]      = { 1, 1, 1, 1 };
//	int boxcol[nl]       = { 6, 12, 14, 3 };
//	double boxscale[nl]  = { 0.8, 0.8, 0.8, 0.8 };
//	int boxlwid[nl]      = { 1, 1, 1, 1 };
//	int boxsty[nl]       = { 8, 0, 0, 0 };
//	int optarray [nl]    = { PL_LEGEND_COLOR_BOX,  
//	                         PL_LEGEND_COLOR_BOX,
//	                         PL_LEGEND_COLOR_BOX,
//	                         PL_LEGEND_COLOR_BOX };
//	int leglinecol[nl]   = { 1, 1, 1, 1 };
//	int leglinesty[nl]   = { 1, 1, 1, 1 };
//	int leglinewid[nl]   = { 4, 2, 2, 2 };
	
	int textcol[nl]      = { 1, 1, 1 };
	int boxcol[nl]       = { 6, 12, 3 };
	double boxscale[nl]  = { 0.8, 0.8, 0.8 };
	int boxlwid[nl]      = { 1, 1, 1 };
	int boxsty[nl]       = { 8, 0, 0 };
	int optarray [nl]    = { PL_LEGEND_COLOR_BOX,  
	                         PL_LEGEND_COLOR_BOX,
	                         PL_LEGEND_COLOR_BOX };
	int leglinecol[nl]   = { 1, 1, 1 };
	int leglinesty[nl]   = { 1, 1, 1 };
	int leglinewid[nl]   = { 4, 2, 2 };

//	Now for the actual legend drawing
	double leg_w, leg_h;
	pls->scol0a( 7, 220, 220, 220, 0.70 );
	pllegend( &leg_w, &leg_h, 
		  PL_LEGEND_BACKGROUND | PL_LEGEND_BOUNDING_BOX,
		  				// PL_LEGEND_BACKGROUND, PL_LEGEND_TEXT_LEFT (opt int)
	          PL_POSITION_LEFT | PL_POSITION_TOP,
	          				// position (opt integer: PL_POSITION_RIGHT, etc.)
	          0.03, 0.03, 0.07, 7,		// x, y offsets, width (fl), background colour (int)
	          7, 1, 0, 0,			// Bounding-box colour, width
	          nl,				// number of legend entries nl (int)
	          optarray,			// array with nl entries setting what to show for
						// each legend entry graphic (here, lines) (int [nl])
	          1.0, 0.85, 2.4,		// text offset, text scale, text spacing (float)
	          0,				// text-justification (0: left, 1: right, etc.) (fl)
	          textcol,			// line colours for graphics with nl entries (int [nl])
	          leg_txt,			// text for description of each leg entry (char *[nl])
	          boxcol, boxsty, boxscale, 	// box colors, box patterns, box scales
	          boxlwid, 			// box line-width
	          leglinecol,			// Array of line-colours (int [nl])
	          leglinesty,			// Array of line-styles (int [nl])
	          leglinewid,			// Array of line-widths (int [nl]))
	          NULL, NULL, NULL, NULL	// Array configuring symbols (not used here) (int [nl])
						// symbol_colors, symbol_scales, symbol_numbers, symbols
	        );
//	End of legend config
	
//	Histograms:
//	Draw only those histograms which lie within the defined viewport
//	Get the min and max xrange for the viewport in x_min and x_max
	double x_min, x_max, tmp;
	pls->gvpw(x_min, x_max, tmp, tmp);

	pls->wid( 2 );
	for(unsigned int j = 0; j < 3; j++)
	{
		for(unsigned int i=0; i<x.size()-1; i++)
		{
//			Check if hist lies in viewport; if not, do nothing
			if (a[i] >= x_min && a[i] <= x_max)
			{
				double tx[4]  = { a[i], a[i], a[i+1], a[i+1] };
				double ty[4]  = { 0, b[j][i], b[j][i], 0 };
				double ttx[2] = { a[i], a[i+1] };
				double tty[2] = { b[j][i], b[j][i] };
				double xx0[2] = { a[i], a[i] };
				double xx1[2] = { a[i+1], a[i+1] };
				double xx2[2] = { 0, b[j][i] };

				if(j)
				{
					pls->psty( 0 );
					pls->col0( boxcol[j] );
				}
				else
				{
					pls->psty( 8 );
					pls->col0( boxcol[j] );
				}
				pls->fill( 4, tx, ty );

				pls->line( 2, xx0, xx2 );
				pls->line( 2, xx0, xx2 );
				pls->line( 2, xx1, xx2 );
				pls->line( 2, xx1, xx2 );
				pls->line( 2, ttx, tty );
				pls->line( 2, ttx, tty );
			}
		}
	}
	pls->wid( 0 );
	
	stringstream ss;
	ss.setf(ios_base::fixed, ios_base::floatfield);
	ss.precision(2);
	double GR_TOT = b[0][x.size()-1] + b[0][x.size()-2];
	ss << GR_TOT;
	
	//~ One may boost::lexical_cast for simple conversion
	//~ but in this case using stringstream provides
	//~ an easier way to set a user-defined precision
	//~ to the numerical value.
	//~ float SB_RATIO = b[y.size()-1][4]/b[3][4];
	//~ string sb_ratio_stri = "S/B = "
		//~ + lexical_cast<string>( SB_RATIO );
	
	const unsigned int lhist = x.size() - 2;
	const double y_off = 0.45;
	double GR_EVE_TEXT_Y = b[0][x.size()-3] * (1 + y_off) ,
	       GR_EVE_TEXT_X = 0.5*(a[lhist] + a[lhist-1]);

	if (b[0][x.size()-2] <= 0.4)
	{
		GR_EVE_TEXT_Y *= 2;
		GR_EVE_TEXT_Y += 0.1;
	}
	pls->schr( 0, 1.2 );
	pls->col0( 6 );
	pls->ptex( GR_EVE_TEXT_X + 0.002, 
	           GR_EVE_TEXT_Y - 0.01,
	           1,
	           0,
	           0,
	           ss.str().c_str() );
	pls->col0( 1 );
	pls->ptex( GR_EVE_TEXT_X, 
	           GR_EVE_TEXT_Y,
	           1,
	           0,
	           0,
	           ss.str().c_str() );
	
	
//	Draw IceCube data points
	double IC_X[x.size()], IC_Y[x.size()];

	for (unsigned int i = 0; i < x.size() - 1; i += 1)
	{
		IC_X[i] = 0.5 * (a[i] + a[i + 1]);
		IC_Y[i] = b[y.size() - 1][i];
	}
	pls->schr( 0, 1.2 );
	pls->string(x.size()-1, IC_X, IC_Y, "\u29fe");
	pls->schr( 0, 0.95 );
	
//	Box placed at the end so that it lies over all other items
	pls->wid( 0 );
	pls->box( "bclnst", 1.0, 0, "bcnstv", 1.0, 0);
	
}
