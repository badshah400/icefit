#!/usr/bin/python
import sys
import os
import argparse
import subprocess as sub
from math import *
import numpy as nm
from scipy.optimize import minimize
import re

parser = argparse.ArgumentParser(description='Evaluating best-fit to IceCube \
                                              data with given spectral index.')
parser.add_argument('index', metavar='alpha', type=float,
                    help = 'Spectral index for fit')
parser.add_argument('-t', "--trim", metavar='T', type=int,
                    help = 'Number of data points to trim (from end)')

args = parser.parse_args()
alp = args.index
trim_len = 0

if (args.trim):
	trim_len = args.trim

wdir = os.getcwd()
fname = wdir + '/cascade_events_sig_back.dat'
if (os.access(fname, os.F_OK)):
	os.remove(fname)

os.chdir(wdir + '/src/')

patt=r"@alpha@"
fr=open('flux.cc.in', 'r')
fw=open('flux.cc', 'w')
for line in fr:
	fw.write(re.sub(patt, str(alp), line))
fr.close()
fw.close()	

sub.call(['make', '-j4'])
sub.call(['./uhecal.out', str(alp)])
os.chdir(wdir)
if (not os.access(wdir + '/results/', os.F_OK)):
	os.mkdir('results')

spectrm23 = nm.loadtxt(fname, skiprows=0, usecols = (0, 2))
ICdat = nm.loadtxt(wdir + "/IC_data/IC_SIGNAL.dat")
if (trim_len >= len(ICdat)):
	print ('FAIL: Attempt to trim more than data length')
	print ('      Data length: ' + str(len(ICdat)))
	print ('      Trim level: ' + str(trim_len))
	sys.exit(-3)

print u'========= alpha =', alp, '========='

datlen = len(ICdat) - trim_len
def riffledarr(a):
	cmb1 = nm.column_stack((spectrm23[0:len(ICdat), 1] * a, ICdat[:, 0]))
	cmb1 = nm.delete(cmb1, range(datlen, len(ICdat)), axis = 0)
	cmb1 = nm.delete(cmb1, (0, 1), axis=0)
	return cmb1

err = nm.array(ICdat[:, 1]) #- ICdat[:, 2])

def fnvar(a):
	tmp = 0
	cmb = riffledarr(a)
	for j in range(0, len(cmb)):
		tmp = tmp + ((cmb[j, 0] - cmb[j, 1])**2)# / err[j]**2)
	return sqrt(tmp)

res = minimize(fnvar, 1.0, method = "Nelder-Mead")
print "Normalisation =", '{:.7g}'.format(res.x[0])
print 'Chisq         =', '{:.3f}'.format(fnvar(res.x[0]))
print ''
print nm.array(['Expected events', 'IC data'])
print riffledarr(res.x[0])

print "Correcting output file..."
fcorr = nm.loadtxt(fname)
for i in range(2, len(fcorr[0])):
	fcorr[:, i] = fcorr[:, i] * res.x[0]
	
fname2 = wdir + "/results/cascade_events_sig_back_" + str(alp) + ".dat"
nm.savetxt(fname2, fcorr, fmt="%.3e", delimiter="\t",
           header="EMIN \t EMAX \t Total \t GR \t BCK1 \t BCK2")
nm.savetxt(str(wdir + "/results/Fit_Chisq_E_" + str(alp) + ".dat"),
           [fnvar(res.x)], fmt="%.3f")
os.remove(fname)
