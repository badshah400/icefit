#!/usr/bin/python

import os
import subprocess as sp

wdir = os.getcwd()
reffile = open(wdir + '/test/ref_2.0.dat')

tmf = os.tmpfile()

sp.check_call(['cp', '-r', 'IC_data', 'test/'])
sp.check_call(['cp', '-r', 'src', 'test/'])
sp.check_call(['cp', 'icefit.py', 'test/'])

os.chdir('test/src')
sp.check_call(['make', 'clean'], stdout = tmf, stderr = tmf)

os.chdir('../')
sp.check_call(['python', 'icefit.py', '2.0', '-t1'],
              stdout = tmf, stderr=tmf)
cmpfile = open('./results/cascade_events_sig_back_2.0.dat')

comp = reffile.read() == cmpfile.read()

if not comp:
	print ('FAIL: Results differ from benchmark')
else:
	print ('PASS: Test passed')

sp.check_call(['rm', '-fr',
               './results', './src', './IC_data',
               './lsq_fit', './icefit.py'],
              stdout = tmf, stderr = tmf)
